import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import registerServiceWorker from './registerServiceWorker';
import Nav from "./components/Nav";
import NavLink from "./components/NavLink";
import Container from "./components/Container";
import Wrapper from "./components/Wrapper";
import ConItem from "./components/ConItem";
import Cover from "./components/Cover";
import ConHeader from "./components/ConHeader";
import Header from "./components/Header";
import ConText from "./components/ConText";
import ConImage from "./components/ConImage";
import Composer from "./components/Composer";

export default function ren(file, data){
    const { type, fid, content, text, src, href } = file;
    switch (type){
        case 0:
            return <Wrapper content={content} key={fid} />;
            break;
        case 1:
            return <Nav content={content} key={fid}  />;
            break;
        case 2:
            return <NavLink text={text} key={fid} href={href} />;
            break;
        case 3:
            return <Container content={content} key={fid} data={data}/>;
            break;
        case 4:
            return <ConItem content={content} key={fid} src={src} />;
            break;
        case 5:
            return <ConImage content={content} key={fid} src={src} />;
            break;
        case 6:
            return <ConText content={content} key={fid} src={src} text={text}/>;
            break;
        case 7:
            return <Composer content={content} key={fid}/>
            break;
        case 12:
            return <Header content={content} key={fid} text={text} fid={fid}/>;
            break;
        default:
            return "oops";
    }
}

    const app = <Wrapper />;


    ReactDOM.render(app, document.getElementById('root'));
    registerServiceWorker();
