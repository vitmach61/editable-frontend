import React, { Component } from "react";

class Header extends Component{

    render(){
        return(
            <div className="container__item container__item--4x1" >
                <div className="heading" >
                    <span className="heading__text" >
                        {this.props.text}
                    </span>
                </div>
            </div>
        );
    }
}

export default Header;