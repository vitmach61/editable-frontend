import React, { Component } from "react";
import ren from "../index";

class Composer extends Component{
    render(){
        const content = this.props.content.map(item=> (
            ren(item)
        ));

        let conClass = "composer";

        return(
            <div className={conClass} onClickCapture={this.setActualHandler}>
                {content}
            </div>
        );
    }
}

export default Composer;