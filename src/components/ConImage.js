import React, { Component } from "react";

class ConImage extends Component{
    render(){
        const style = {order: 0};

        const src = "http://" + process.env.REACT_APP_PATH + "/uploads/" + this.props.src;


        return(
            <div className="imageCon">
                <img alt="sorry no image" src={src} />
            </div>
        );
    }
}

export default ConImage;