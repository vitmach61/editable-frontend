import React, { Component } from "react";
import ren from "../index";

class Container extends Component{
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    componentDidMount() {
        const url ="http://" + process.env.REACT_APP_PATH + "/elems/content/" + this.props.data;
        fetch(url)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
    }

    render(){
        let content;
        if(this.state.isLoaded && this.state.items.sendData.content !== undefined){
            content = this.state.items.sendData.content.map((item) => {
                if (item != null || item !== undefined)
                    return(
                        <div className="container__item" key={item.fid}>
                            {ren(item)}
                        </div>
                    );
            });
        }

        return(
            <div className="container">
                {content}
            </div>
        );
    }
}

export default Container;