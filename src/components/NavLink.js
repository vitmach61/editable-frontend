import React, { Component } from "react";
import {Link} from "react-router-dom";

class NavLink extends Component{
    render(){

        return(
            <Link to={"/"+this.props.href} className="nav__item">
                {this.props.text}
            </Link>
        );
    }
}

export default NavLink;
