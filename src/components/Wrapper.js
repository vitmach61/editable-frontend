import React, { Component } from "react";
import ren from "../index";
import {BrowserRouter as Router, Link, Route, Redirect} from "react-router-dom";

import Container from "./Container";

class Wrapper extends Component{
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: [],
            data: ""
        };

        this.Child = this.Child.bind(this);
        this.setWindowSize = this.setWindowSize.bind(this);
    }
    componentDidMount() {
        fetch("http://" + process.env.REACT_APP_PATH + "/elems/page")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: false,
                        error
                    });
                }
            );

        window.addEventListener("resize", this.setWindowSize);
        this.setWindowSize();
    }

    setWindowSize() {
        const myWidth = window.innerWidth;
        const myHeight = window.innerHeight;
        if(myWidth > 768 ) {
            this.setState({window: 1});
        } else {
            this.setState({window: 0});
        }
    }


    Child({ match }) {
        return(
            <Container data={match.params.id} key={match.params.id}/>

        );
    }

    render(){
        if(this.state.isLoaded){
            const content = this.state.items.sendData.content.map(item=> {
                if(item != null)
                    return(ren(item ));
                else{
                    return <Redirect from="/" to="home"/>;
                }
            });
            let wrapperClass;
            if(this.state.window){
                wrapperClass = "wrapper wrapper--nsm";
            } else {
                wrapperClass = "wrapper";
            }


            return(
                <Router>
                    <div className={wrapperClass}>
                        {content}
                        <Route path="/:id" component={this.Child} />
                    </div>
                </Router>
            );
        } else {
            return("error: no data :(");
        }
    }
}

export default Wrapper;