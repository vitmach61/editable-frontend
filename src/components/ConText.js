import React, { Component } from "react";

class ConText extends Component{
    render(){

        const style = {order: 0};

        return(
            <div className="textCon" style={style}>
                <span>
                   {this.props.text}
                </span>
            </div>
        );
    }
}

export default ConText;