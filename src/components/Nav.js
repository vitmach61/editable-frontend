import React, { Component } from "react";
import ren from "../index";
import {BrowserRouter as Router, Link, Route} from "react-router-dom";

class Nav extends Component{
    constructor(props){
        super(props);

        this.state = {
            toggle: false
        };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(){
        this.setState({toggle: !this.state.toggle});
    }

    render(){

        const items = this.props.content.map(item=>(
            ren(item)
            //console.log(item)
        ));

        const listClasses = (this.state.toggle)? "nav__con nav__con--open" : "nav__con";

        const list = <div className={listClasses}>{items}</div>;

        const toggleButton = (5>3)?
            <div className="menuToggle menuToggle">
                <input type="checkbox" checked={this.state.toggle} onChange={this.handleChange}/>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            : "";

        return(
            <nav className="nav">{list} {toggleButton}</nav>
        )
    }
}


export default Nav;