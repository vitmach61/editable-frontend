import React, { Component } from "react";
import ren from "../index.js";

class ConItem extends Component{
    render(){
        const content = this.props.content.map(item => (
            <div className="container__item__child" key={item.fid}>
                {ren(item)}
            </div>
        ));
        const style = {order: 0};
        return(
            <div className="container__item container__item--4x2" style={style}>
                {content}
            </div>
        );
    }
}

export default ConItem;